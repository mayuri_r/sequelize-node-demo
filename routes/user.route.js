const userController = require('../controllers/users.controllers')
const asyncWrap = require('express-async-wrapper')
const express = require('express')
const app = express()

app.get('/api', asyncWrap(userController.getUser))
app.get('/api/:userId', asyncWrap(userController.getUserId))
app.post('/api/createuser', asyncWrap(userController.createuser))
app.put('/api/updateUser/:userId', asyncWrap(userController.updateUser))
app.delete('/api/deleteUser/:userId', asyncWrap(userController.deleteUser))
app.post('/api/addUserHobbies/:userId', asyncWrap(userController.addUserHobbies))
app.get('/api/user/hobies', asyncWrap(userController.hobbies))
app.get('/api/user/hoby/:userId',asyncWrap(userController.hobyByUser))
app.delete('/api/deleteUserHobby/:userId/:hobbyId',asyncWrap(userController.deleteUserHobby))

module.exports = app