'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_hobbies extends Model {
  
    static associate(models) {
      user_hobbies.belongsTo(models.User, {
        foreignKey: 'userId',
      });
    }
  };

  user_hobbies.init({
    userId: DataTypes.INTEGER,
    hobbyName: DataTypes.STRING,
    deletedAt: DataTypes.DATE    

  }, {
    sequelize,
    paranoid: true,
    modelName: 'user_hobbies',
  });
  return user_hobbies;
};