# sequelize-node-demo



## Getting started

CRUD operations using sequlize ,postgresql and node js

## Project Status

Completed

## create migration

sequelize init --> for create model structure

npx sequelize-cli model:generate --name User --attributes firstName:string,lastName:string,email:string --> for create table

npx sequelize-cli db:migrate --> to run migration

npx sequelize-cli db:migrate --to filename.js -> to run one migration

## join

*in migration file*
using migration
**add refrence table with it's primary key
 userId: {
        type: Sequelize.DataTypes.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'Users',
          key: 'id',
        }
      }

*in model file*
using migration
example:
    current_model.belongsTo(refrence_model_name, {
        foreignKey: 'userId',
      });

*in service file*
using migration
example:
    include:[{
        model:"refrencetablename",
        attributes(if required)
    }]

## delete
*paranoid*
soft delete 
    paranoid:true

hard delete
    paranoid:true
    force:true


