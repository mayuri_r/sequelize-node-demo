// import express from 'express';
const express = require('express')
const userRoute = require('./routes/user.route')
require('./models/index')
const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: true }));


app.use('/', userRoute);

app.listen(4000, () => console.log("listion on servre 4000"))