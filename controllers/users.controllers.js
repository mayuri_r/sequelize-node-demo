const res = require('express/lib/response')
const userService = require('../services/user.services')

async function getUser(req, res) {
    const userList = await userService.getUser()
    res.json({
        message: "users list",
        userList
    })
}

async function getUserId(req, res) {
    const [user] = await userService.getUserId(req.params.userId)
    res.json({
        message: "user " + user.firstName,
        user
    })
}

async function createuser(req, res) {
    await userService.createUser(req.body)
    res.json({
        "message": "user created sucessfully"
    })
}

async function updateUser(req, res) {
    await userService.updateUser(req.params.userId, req.body)
    res.json({
        "message": "user update sucessfully"
    })
}

async function deleteUser(req, res) {
    await userService.deleteUser(req.params.userId)
    res.json({
        "message": "user deleted sucessully"
    })
}

async function addUserHobbies(req, res) {
    await userService.addUserHobbies(req.params.userId, req.body)
    res.json({
        "message": "user hobbies added suceessfully"
    })
}

async function hobbies(req, res) {
    const data = await userService.hobbies()
    res.json({
        "message": "UserList",
        data
    })

}

async function hobyByUser(req,res) {
    const data = await userService.hobyByUser(req.params.userId)
    res.json({
        message:"user",
        data
    })
}

async function deleteUserHobby(req,res) {
    await userService.deleteUserHobby(req.params)
    res.json({
        message:"hobby deleted sucessfully"
    })
}

module.exports = {
    getUser,
    getUserId,
    createuser,
    updateUser,
    deleteUser,
    addUserHobbies,
    hobbies,
    hobyByUser,
    deleteUserHobby
}