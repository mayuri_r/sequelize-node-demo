const {
    User,
    user_hobbies
} = require('../models/index')

async function getUser() {
    const users = await User.findAll()
    return users
}

async function getUserId(userId) {
    const [user] = await User.findAll(({
        where: {
            id: userId
        }
    }))
    if (!user) {
        throw new Error('user not found')
    }
    return user
}

async function createUser(data) {
    return User.create({
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email
    })
}

async function updateUser(userId, data) {
    const [user] = await User.findAll(({
        where: {
            id: userId
        }
    }))
    if (!user) {
        throw new Error('user not found')
    }

    return User.update({
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email
    }, {
        where: {
            id: userId
        }
    })
}

async function deleteUser(userId) {
    const [user] = await User.findAll(({
        where: {
            id: userId
        }
    }))
    if (!user) {
        throw new Error('user not found')
    }
    return User.destroy({
        where: {
            id: userId
        }
    })
}

async function addUserHobbies(userId, data) {
    return await user_hobbies.create({
        userId,
        hobbyName: data.hobby
    })
}

async function hobbies() {
    const users = await user_hobbies.findAll()
    return users
}

async function hobyByUser(userId) {
    const user = await user_hobbies.findAll(({
        attributes:['userId','hobbyName'],
        where: {
            userId
        },
        include:[{
            model:User,
            attributes:['firstName','lastName'],
            required:false
        }]
    }))
    if (!user) {
        throw new Error('user not found')
    }
    return user
}

async function deleteUserHobby(data) {
    const [user] = await user_hobbies.findAll(({
        where: {
            id: data.hobbyId,
            userId:data.userId
        }
    }))
    if (!user) {
        throw new Error('user not found')
    }
    return user_hobbies.destroy({
        where: {
            id: data.hobbyId,
            userId:data.userId
        },
        // force: true WHEN DO HARD DELETE

    })
}



module.exports = {
    getUser,
    getUserId,
    createUser,
    updateUser,
    deleteUser,
    addUserHobbies,
    hobbies,
    hobyByUser,
    deleteUserHobby
}